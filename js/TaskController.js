app.controller('taskController', function ($scope) {
    $scope.tasks = [
        {text: 'Hello, World!', done: false, editing: false, hidden: false},
        {text: 'I love JS!', done: false, editing: false, hidden: false},
        {text: 'FFFFFFFFF', done: false, editing: false, hidden: false},
        {text: 'I am hungry', done: false, editing: false, hidden: false},
        {text: 'ya love sirniki', done: false, editing: false, hidden: false}
    ];

    $scope.addTask = function () {
        $scope.tasks.push({text: $scope.formTodoText, done: false, editing: false});
    };

    $scope.editTask = function (item) {
        item.editing = true;
    };

    $scope.doneEditing = function (item) {
        item.editing = false;
        //dong some background ajax calling for persistence...
    };

    $scope.deleteTask = function () {
        var oldTodos = $scope.tasks;
        $scope.tasks = [];
        angular.forEach(oldTodos, function (task) {
            console.log(task);
            if (!task.done) $scope.tasks.push(task);
        });
    };

    $scope.hideTask = function () {
        var oldTodos = $scope.tasks;
        $scope.tasks = [];
        angular.forEach(oldTodos, function (task) {
            console.log(task);
            if (!task.done) task.hidden = true;
        });
    }
})